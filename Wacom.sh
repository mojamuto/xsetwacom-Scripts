#! /bin/bash
# Setup xsetwacom script for Huion New 1060PLUS (8192)
# License: CC-0/Public-Domain license

# Script para la configuración de xsetwacom para Huion New 1060PLS (8192)

# Definición de la tableta
tabletstylus="HID 256c:006e stylus"
tabletpad="HID 256c:006e Pad pad"
touchpad="HID 256c:006e Touch Strip pad" #sin soporte

# Mostrar todas las opciones disponibles:
#xsetwacom get "$tabletstylus" all
#xsetwacom get "$tabletpad" all
#xsetwacom get "$touchpad" all

# Reiniciar
xsetwacom --set "$tabletstylus" ResetArea

# Mapea el área de trabajo de la tableta a un monitor en caso de tener múltiples
# Nota: Para obtener el nombre del monitor al que desean asociar el área activa de la tableta debe utilizar xrandr.
# Monitor HDMI=HDMI-A-0 / Monitor del Portatil=eDP
#
# Hay un viejo bug con los drivers de nvidia, y puede no encontrar el monitor con la salida que da xrandr, xsetwacom -v puede dar mas detalles,
# en ese caso usa "HEAD-n" donde "n" es el numero del monitor.
xsetwacom --set "$tabletstylus" MapToOutput "HEAD-1" 

# Mapeo auto proporcional, esto se puede ejecutar antes para obtener el valor por defecto:
# xsetwacom get "$tabletstylus" Area
#
# por defecto: 0 0 68840 38720
# Agregue abajo la resolución del monitor donde utilizará la tableta gráfica:
screenX=1920
screenY=1080
Xtabletmaxarea=50800
Ytabletmaxarea=31750
areaY=$(( $screenY * $Xtabletmaxarea / $screenX ))
xsetwacom --set "$tabletstylus" Area 0 0 "$Xtabletmaxarea" "$areaY"
#


# Botones del lápiz por si se quiere cambiar el comportamiento por defecto:
#
#		|   _   |
#		|  | |  |
#		|  |3|  |	
#		|  |_|  |
#		|  | |  |
#		|  |2|  |
#		|  |_|  |
#		\	/
#		 \     /
#		  \   /
#		   \ /
#		    V  --> 1
#
#xsetwacom --set "$tabletstylus" Button 1 1 			# por defecto, clic para dibujar
#xsetwacom --set "$tabletstylus" Button 2 2 		  	# por defecto clic derecho			
#xsetwacom --set "$tabletstylus" Button 3 "key Control_L" 	# Ctrl = selector de color		
#				


# Mejoras
#
# Curva de presión del lápiz:
xsetwacom --set "$tabletstylus" PressureCurve 0 0 100 100
#
# Curva de presión más suave para el lápiz:
#xsetwacom --set "$tabletstylus" PressureCurve 0 10 40 85

# Configuración de recorte y supresión de datos: (esta parte no la entiendo muy bien
# así que la dejo como aparece en el script)
# The event of this device are good; if you have CPU better to not filter
# them at operating system level to not loose any sensitivity.
# Minimal trimming is also good.
xsetwacom --set "$tabletstylus" Suppress 0 # data pt.s filtered, default is 2, 0-100
xsetwacom --set "$tabletstylus" RawSample 1 # data pt.s trimmed, default is 4, 1-20

# Modo para usuarios zurdos (rotación):
#xsetwacom --set "$tabletstylus" Rotate half

# ** Botones **
#
#
#     +-----++-----+
#     |  1  ||  2  |
#     +-----++-----+
#     
#     +-----++-----+
#     |  3  ||  8  |
#     +-----++-----+
#     
#     +-----++-----+
#     |  9  || 10  |
#     +-----++-----+

#	     H
#	     U
#	     I
#	     O
#	     N
#
#     +-----++-----+
#     |  11 || 12  |
#     +-----++-----+
#
#     +-----++-----+
#     | 13  |  14  |
#     +-----++-----+
#
#     +-----++-----+
#     |  15 || 16  |
#     +-----++-----+

# ** Atajos comunes para Krita **
#
# ** Un signo + antes de una tecla significa que se mantiene presionada, mientras que un signo - significa que se suelta.
# ** Las teclas sin signos se presionan y sueltan.
#
#xsetwacom --set "$tabletpad" Button 1 "key Control_L" 		# Ctrl = Selector de color
#xsetwacom --set "$tabletpad" Button 2 "key KP_Divide" 		# / = Cambiar a la herramienta de dibujo previamente utilizada
#xsetwacom --set "$tabletpad" Button 3 "key Shift_L" 		# Shift = Cambiar tamaño del pincel
#xsetwacom --set "$tabletpad" Button 8 "key m" 			# m = Modo espejo
#xsetwacom --set "$tabletpad" Button 9 "key e" 			# e = Cambiar al borrador
#xsetwacom --set "$tabletpad" Button 10 "key Ctrl z" 		# Ctrl + Z = Deshacer la última acción
#


# Opcional, evitar asignarle a este botón comandos que requieran mantenerlo presionado
# por largo tiempo, para evitar activar el menú OSD por error.
# xsetwacom --set "$tabletpad" Button 11 "key Ctrl 0" # Ctrl + 0 = Reiniciar zum