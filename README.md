
                       Script para utilizar la tableta Huion 1060Plus con xsetwacom


__Como utilizarlo:__
====================

1. Instalar DIGImend kernel drivers: [DIGImend-kernel-drivers](https://github.com/DIGImend/digimend-kernel-drivers)
2. Clona el repositorio
3. Da permisos de ejecucion al script ( chmod +x <nombre de script> )
4. Ejecuta el Script


__Notas__
=========
```
- Hay que ejecutar el script cada vez que se reinicia o apaga el pc
```